import React from 'react';
import { Alert, Dimensions, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import MapView from 'react-native-maps';
import { Icon } from 'react-native-elements';
import { Bubbles } from 'react-native-loader';

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.002;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const API_IP = 'https://harmony-api.herokuapp.com';

export default class App extends React.Component {
  state = {
    latitude: null,
    longitude: null,
    visible: false
  }
  
  componentDidMount() {
    this.watchId = navigator.geolocation.watchPosition(
      (position) => {
        this.setState({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          error: null,
        });
      },
      (error) => this.setState({ error: error.message }),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000, distanceFilter: 10 },
    );
  }

  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchId);
  }
  
  _handleButtonPress = () => {
    this.setState({visible: true})
    fetch(API_IP+'/location', {  
      method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            latitude: this.state.latitude,
            longitude: this.state.longitude,
      })
    })
    .then((response) => {
      this.setState({visible: false})
      if (response.status === 200) {
        Alert.alert(
          'Location submitted',
          'Details available at harmony-api.herokuapp.com',
        );
      }
      else {
        Alert.alert(
          'An error has occured',
          'Please try again later',
        );
      }
    })
    .catch((error) => {
      console.error(error);
      this.setState({visible: false})
    });
  }

  render() {
    if(!this.state.latitude || !this.state.longitude){
      return(
        <View style={{flex: 1}}>
          <Text>Location is loading...</Text>
        </View>
      );
    }

    return (
      <View style={styles.container}>
        <View style={styles.heading}>
          <Text>Location Services Demo</Text>
        </View>
        <MapView
          style={{flex:2}}
          region={{
                  longitudeDelta: LONGITUDE_DELTA,
                  latitudeDelta: LATITUDE_DELTA,
                  latitude:parseFloat(this.state.latitude),
                  longitude:parseFloat(this.state.longitude)
                 }}
          showsUserLocation={true}
          showsMyLocationButton={true}
        />
        <View style={styles.bottom}>
          {this.state.visible ? 
            <Bubbles 
              size={10} 
              color="#000" 
            />
            :
            <TouchableOpacity style={{alignSelf: 'center'}} onPress={this._handleButtonPress}>
              <View style={styles.buttoncanvas}>
                <View style={styles.buttonicon}>
                  <Icon
                    name={'location-on'}
                    color={'white'}
                    size={32}
                  />
                </View>
                <View style={styles.buttontext}>
                  <Text style={styles.buttontextfont}>SUBMIT LOCATION</Text>
                </View>
              </View>
            </TouchableOpacity>
          }
        </View>      
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center'
  },
  heading: {
    flex: 0.2, 
    marginTop: 40, 
    alignItems: 'center'
  },
  bottom: {
    alignItems: 'center', 
    flex: 1, 
    marginTop: 30
  },
  buttoncanvas: {
    height:60, 
    width: 220, 
    flexDirection: 'row', 
    alignItems: 'stretch'
  },
  buttonicon: {
    backgroundColor: 'black', 
    flex: 1, 
    alignItems:'flex-end', 
    justifyContent:'center'
  },
  buttontext: {
    backgroundColor: 'black', 
    flex: 3, 
    alignItems:'flex-start', 
    justifyContent:'center', 
    padding: 5
  },
  buttontextfont: {
    fontSize: 14, 
    color: 'white', 
    fontWeight: 'bold'
  }
});
