Location Services Demo App bootstrapped with [Create React Native App](https://github.com/react-community/create-react-native-app).

I have provided a link in the email which contains a QR code. Please install Expo Client on your test device and scan the QR code to install the app.

Please contact me for any assistance.